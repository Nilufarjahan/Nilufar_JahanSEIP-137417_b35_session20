<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/27/2016
 * Time: 7:59 PM
 */

namespace App;


class Student extends Person
{
    public $studentId="SEIP-137417";
    public function showStudentInfo()
    {
        parent::showPersonInfo();
        echo $this->studentId;
    }
}