<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 10/27/2016
 * Time: 7:59 PM
 */

namespace App;


class Person
{

    public $name="Nilufar";
    public $gender="Female";
    public $blood_group="O+";
    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}